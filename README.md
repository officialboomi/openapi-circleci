# CircleCI Connector
The CircleCI API is a full-featured RESTful API that allows you to access all information and trigger all actions in CircleCI.

Documentation: https://circleci.com/docs/api/v2/
Specification: https://github.com/Enspire-Tech/openapi-connector-artifacts/blob/master/circleci/custom-specification-circleci.json

## Prerequisites

+ CircleCI account
+ Personal API token

## Supported Operations

**6 out of 41 endpoints are failing.**

The following operations are **not** supported at this time:
* getCollaborations
* getTests
* getJobDetails
* getPipelineByNumber
* getJobDetails
* getJobArtifacts


## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

